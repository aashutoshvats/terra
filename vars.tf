variable "subnet" {
  type = map(any)
  default = {
    "vpcsubnet"              = "10.0.0.0/16"
    "publicSubnet1"          = "10.0.0.0/25"
    "publicSubnet2"          = "10.0.0.128/25"
    "privateSubnet1"         = "10.0.1.0/25"
    "privateSubnet2"         = "10.0.1.128/25"
    "destination_cidr_block" = "0.0.0.0/0"
   # "ipv4Public"             = aws_instance.ec2pub1.private_ip

  }
}

variable "name" {
  type = map(any)
  default = {
    "vpcName"                    = "ninja-vpc-01"
    "publicSubnet1Name"          = "ninja-pub-sub-01"
    "publicSubnet2Name"          = "ninja-pub-sub-02"
    "privateSubnet1Name"         = "ninja-priv-sub-01"
    "privateSubnet2Name"         = "ninja-priv-sub-02"
    "IGWName"                    = "ninja-igw-01"
    "NATName"                    = "ninja-nat-01"
    "publicSubnetRoutTableName"  = "ninja-route-pub-01/02"
    "privateSubnetRoutTableName" = "ninja-route-priv-01/02"

  }
}


