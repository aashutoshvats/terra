#VPC

resource "aws_vpc" "terraformVpc" {
  cidr_block       = var.subnet.vpcsubnet
  instance_tenancy = "default"

  tags = {

    name = var.name.vpcName
  }
}

# Public Subnet 1

resource "aws_subnet" "publicSubnet1" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.publicSubnet1
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = var.name.publicSubnet1Name
  }
}

# Public Subnet 2

resource "aws_subnet" "publicSubnet2" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.publicSubnet2
  availability_zone       = "ap-south-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = var.name.publicSubnet2Name
  }
}

# Private Subnet 1

resource "aws_subnet" "privateSubnet1" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.privateSubnet1
  availability_zone       = "ap-south-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = var.name.privateSubnet1Name
  }
}

# Private Subnet 2

resource "aws_subnet" "privateSubnet2" {
  vpc_id                  = aws_vpc.terraformVpc.id
  cidr_block              = var.subnet.privateSubnet2
  availability_zone       = "ap-south-1b"
  map_public_ip_on_launch = false

  tags = {
    Name = "var.name.privateSubnet2Name"
  }
}

#Internet Gateway

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.terraformVpc.id
  tags = {
    Name = var.name.IGWName
  }


}

# Public Route Table

resource "aws_route_table" "publicRoutTable" {
  vpc_id = aws_vpc.terraformVpc.id
  tags = {
    Name = var.name.publicSubnetRoutTableName
  }
}

# Public Route Table Routes

resource "aws_route" "publicRoutes" {
  route_table_id         = aws_route_table.publicRoutTable.id
  destination_cidr_block = var.subnet.destination_cidr_block
  gateway_id             = aws_internet_gateway.IGW.id
}

# Subnet Association

resource "aws_route_table_association" "publicSubnetAssociation1" {
  subnet_id      = aws_subnet.publicSubnet1.id
  route_table_id = aws_route_table.publicRoutTable.id
}

resource "aws_route_table_association" "publicSubnetAssociation2" {
  subnet_id      = aws_subnet.publicSubnet2.id
  route_table_id = aws_route_table.publicRoutTable.id
}

# Generate Elastic Ip

resource "aws_eip" "NAT_eip" {
  vpc = true
}

# NAT Gateway

resource "aws_nat_gateway" "NAT" {
  allocation_id = aws_eip.NAT_eip.id
  subnet_id     = aws_subnet.publicSubnet1.id
  tags = {
    Name = var.name.NATName
  }
}


# Private Route Table

resource "aws_route_table" "privateRoutTable" {
  vpc_id = aws_vpc.terraformVpc.id
  tags = {
    Name = var.name.privateSubnetRoutTableName
  }
}

# Private Route Table Routes

resource "aws_route" "privateRoutes" {
  route_table_id         = aws_route_table.privateRoutTable.id
  destination_cidr_block = var.subnet.destination_cidr_block
  nat_gateway_id         = aws_nat_gateway.NAT.id
}

# SUbnet Association

resource "aws_route_table_association" "privateSubnetAssociation" {
  subnet_id      = aws_subnet.privateSubnet1.id
  route_table_id = aws_route_table.privateRoutTable.id
}

resource "aws_route_table_association" "privateSubnetAssociation2" {
  subnet_id      = aws_subnet.privateSubnet2.id
  route_table_id = aws_route_table.privateRoutTable.id
}

# ************************************************************************************

#Public SG

resource "aws_security_group" "publicSubnet" {
  # ... other configuration ...
   vpc_id = aws_vpc.terraformVpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["103.46.203.195/32"]
  }


egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }  
    tags = {

    Name = "public Subnet SG"
  }


}

#Public Instance 1


resource "aws_instance" "ec2pub1" {
  ami           = "ami-0db8b6ca6bb94a2de" 
  instance_type = "t2.micro"
  key_name      =  "vpcAssignment1"
  vpc_security_group_ids = [aws_security_group.publicSubnet.id]

subnet_id       =  aws_subnet.publicSubnet1.id
 tags = {

    Name = "ec2 pub1"
  }
}

#Private SG

resource "aws_security_group" "privateSubnet" {
  # ... other configuration ...
   vpc_id = aws_vpc.terraformVpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.119/32"]
  }
                                 ##   aws_instance.ec2pub1.private_ip

egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
    tags = {

    Name = "private Subnet SG"
  }


}

#Public Instance 2


resource "aws_instance" "ec2pub2" {
  ami           = "ami-0db8b6ca6bb94a2de"
  instance_type = "t2.micro"
  key_name      =  "vpcAssignment1"
  vpc_security_group_ids = [aws_security_group.publicSubnet.id]

 subnet_id       =  aws_subnet.publicSubnet2.id
 tags = {

    Name = "ec2 pub2"
  }
}

#private Instance 1

resource "aws_instance" "ec2pri1" {
  ami           = "ami-0db8b6ca6bb94a2de"
  instance_type = "t2.micro"
  key_name      =  "vpcAssignment1"
  vpc_security_group_ids = [aws_security_group.privateSubnet.id]

  subnet_id       =  aws_subnet.privateSubnet1.id
  tags = {

    Name = "ec2 pri1"
  }
}

#private Instance 2

resource "aws_instance" "ec2pri2" {
  ami           = "ami-0db8b6ca6bb94a2de"
  instance_type = "t2.micro"
  key_name      =  "vpcAssignment1"
  vpc_security_group_ids = [aws_security_group.privateSubnet.id]

  subnet_id       =  aws_subnet.privateSubnet2.id
  tags = {

    Name = "ec2 pri2"
  }
}

